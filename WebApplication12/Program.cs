using Microsoft.EntityFrameworkCore;
using System;
using WebApplication12.Models;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
string connection = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseMySQL(connection));

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

    if (!dbContext.Companies.Any())
    {
        dbContext.Companies.AddRange(
                      new Company { Name = "Company1", Location = "Location1", EmployeesCount = 100 },
                      new Company { Name = "Company2", Location = "Location2", EmployeesCount = 200 },
                      new Company { Name = "Company3", Location = "Location3", EmployeesCount = 150 },
                      new Company { Name = "Company4", Location = "Location4", EmployeesCount = 300 },
                      new Company { Name = "Company5", Location = "Location5", EmployeesCount = 50 }
                  );

        dbContext.SaveChanges();
    }
}

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
